import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyCMgxNZuog-Ow5Q1XnvBmYbKw6ybpVC6MU",
    authDomain: "sugarfree-d50de.firebaseapp.com",
    databaseURL: "https://sugarfree-d50de-default-rtdb.firebaseio.com",
    projectId: "sugarfree-d50de",
    storageBucket: "sugarfree-d50de.appspot.com",
    messagingSenderId: "315875490214",
    appId: "1:315875490214:web:f94017fcadb985459a7767"
};

firebase.initializeApp(firebaseConfig)

const auth = firebase.auth()
const db = firebase.firestore()


const usersCollection = db.collection('users')

export {
    auth,
    db,
    usersCollection
}